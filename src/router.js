import Vue from 'vue';
import Router from 'vue-router';
import MainNavbar from './layout/MainNavbar.vue';
import MainNavbarUser from './layout/MainNavbarUser.vue';
import MainNavbarResearch from './layout/MainNavbar_researcher.vue';
import MainNavbarAdmin from './layout/MainNavbarAdmin.vue';
import MainFooter from './layout/MainFooter.vue';
import Index from './pages/Index.vue';
import Landing from './pages/Landing.vue';
import Login from './pages/Login.vue';
import Admin from './pages/Profile.vue';
import AllDataset from './pages/AllDataset.vue'
import DatasetDetail from './pages/DatasetDetail.vue'
import Organization from './pages/Organization.vue'
import ResearchGroup from './pages/ResearchGroup.vue'

// Reasercher & admin Page

import Researcher from './pages/researchers/researcher.vue'
import Dashboard from './pages/researchers/dashboard.vue'
import Form from './pages/researchers/form.vue'
import Edit from './pages/researchers/Editform.vue'
import ProfileUser from './pages/researchers/profile.vue'
import Account from './pages/researchers/studAccount.vue'

//  test 
import test from './test.vue';


Vue.use(Router);

export default new Router({
  linkExactActiveClass: 'active',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      components: { default: Index, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/test',
      name: 'test',
      components: { default: test },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      }
    },

    {
      path: '/login',
      name: 'login',
      components: { default: Login, header: MainNavbar },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: '/admin',
      name: 'admin',
      components: { default: Admin, header: MainNavbarAdmin },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/alldataset',
      name: 'alldataset',
      components: { default: AllDataset, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 250 },
        footer: { backgroundColor: 'black' }
      },
    },
    {
      path: '/datadetail',
      name: 'datadetail',
      components: { default: DatasetDetail, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      },
    },
    {
      path: '/organization',
      name: 'organization',
      components: { default: Organization, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 250 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/researchgroup',
      name: 'researchgroup',
      components: { default: ResearchGroup, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 250 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/studentAccount',
      name: 'studentAccount',
      components: { default: Account, header: MainNavbarUser },
      props: {
        header: { colorOnScroll: 250 },
        footer: { backgroundColor: 'black' }
      }
    },
    {
      path: '/researcher',
      name: 'researcher',
      redirect: '/researcher/dashboard',
      components: { default: Researcher, header: MainNavbarResearch },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: 'black' }
      },
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          components: { default: Dashboard, header: MainNavbarResearch },
        },
        {
          path: 'form',
          name: 'form',
          components: { default: Form, header: MainNavbarResearch },
        },
        {
          path: 'edit',
          name: 'edit',
          components: { default: Edit, header: MainNavbarResearch },
        },
        {
          path: 'profileuser',
          name: 'profileuser',
          components: { default: ProfileUser, header: MainNavbarResearch },
        },
      ],
    },
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
