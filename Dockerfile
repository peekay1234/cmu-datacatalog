FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . /app
RUN npm run build

FROM nginx:alpine as production-stage
COPY --from=build-stage /app/dist/ /usr/share/nginx/html/app/
COPY ./app.conf /etc/nginx/conf.d/
# Production 
# FROM node:lts-alpine as build-stage
# WORKDIR /app
# ENV PATH /app/node_modules/.bin:$PATH/
# COPY package.json /app/package.json
# RUN npm install --silent
# RUN npm install @vue/cli -g
# COPY . /app
# RUN npm run build

# ENV HOST 0.0.0.0